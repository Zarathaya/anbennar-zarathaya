﻿1 = {
	name = "dynn_Silmuna"
	culture = "east_damerian"
}

2 = {
	name = "dynn_Siloriel"
	culture = "high_lorentish"
}

3 = {
	name = "dynn_Silgarion"
	culture = "east_damerian"
}

4 = {
	name = "dynn_Silistra"
	culture = "east_damerian"
}

5 = {
	name = "dynn_Silurion"
	culture = "east_damerian"
}

6 = {
	name = "dynn_Silcalas"
	culture = "east_damerian"
}

7 = {
	name = "dynn_Silebor"
	culture = "east_damerian"
}

8 = {
	name = "dynn_Silnara"
	culture = "east_damerian"
}

9 = {
	name = "dynn_Pearlman"
	culture = "pearlsedger"
}

10 = {
	prefix = "dynnp_sil"
	name = "dynn_Roilsarding"
	culture = "roilsardi"
}

11 = {
	prefix = "dynnp_sil"
	name = "dynn_Vis"
	culture = "halfling"
}

12 = {
	prefix = "dynnp_sil"
	name = "dynn_Deranne"
	culture = "derannic"
}

13 = {
	prefix = "dynnp_sil"
	name = "dynn_Sorncost"
	culture = "sorncosti"
}

14 = {
	name = "dynn_Rubentis"
	culture = "high_lorentish"
}

15 = {
	name = "dynn_jaherzuir"
	culture = "sun_elf"
}

16 = {
	name = "dynn_Redstone"
	culture = "ruby_dwarf"
}

18 = {
	prefix = "dynnp_sil"
	name = "dynn_Uelaire"
	culture = "east_damerian"
}

19 = {
	name = "dynn_Roysfort"
	culture = "halfling"
}