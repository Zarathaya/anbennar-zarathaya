### Duchy of Upper Bloodwine ###
#County of Upper Bloodwine
74 = { #Kyliande

    # Misc
    culture = high_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Palevine
114 = { #Palevine

    # Misc
    culture = low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Greenfield
108 = { #Greenfield

    # Misc
    culture = low_lorentish
    religion = house_of_minara
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Nurionn
95 = { #Nurionn

    # Misc
    culture = high_lorentish
    religion = house_of_minara
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

### Duchy of Enteben ###
#County of Enteben
96 = { #Enteben

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Saddleglade
98 = { #Saddleglade

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

### Duchy of Crovania ###
#County of New Adea
82 = { #New Adea

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Crovan's Rest
83 = { #Crovan's Rest

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Mistspear
106 = { #Mistspear

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

### Duchy of Venail ###
#County of Venail
93 = { #Venail

    # Misc
    culture =  moon_elf
    religion = court_of_adean #Placeholder
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Firstsight
127 = { #Firstsight

    # Misc
    culture =  moon_elf
    religion = court_of_adean #Placeholder
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Edilliande
94 = { #Edilliande

    # Misc
    culture =  moon_elf
    religion = court_of_adean #Placeholder
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

### Duchy of Great Ording ###
#County of High Ording
75 = { #High Ording

    # Misc
    culture =  low_lorentish
    religion = court_of_adean 
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of East Ording
92 = { #East Ording

    # Misc
    culture =  low_lorentish 
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of West Ording
107 = { #West Ording

    # Misc
    culture =  low_lorentish #Placeholder
    religion = court_of_adean #Placeholder
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

### Duchy of Redglades ###
#County of Iorellen
115 = { #Iorellen

    # Misc
    culture =  moon_elf
    religion = court_of_adean #Placeholder
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Springlade
117 = { #Springlade

    # Misc
    culture =  moon_elf
    religion = court_of_adean #Placeholder
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Summerglade
118 = { #Summerglade

    # Misc
    culture =  moon_elf
    religion = court_of_adean #Placeholder
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Winterglade
120 = { #Winterglade

    # Misc
    culture =  moon_elf
    religion = court_of_adean #Placeholder
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Autumnglade
116 = { #Autumnglade

    # Misc
    culture =  moon_elf
    religion = court_of_adean #Placeholder
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}
### Duchy of Lorentaine ###
#County of Lorentaine
67 = { #Lorentaine

    # Misc
    culture =  high_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Rewanfork
69 = { #Rewanfork

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Ionnidar
70 = { #Ionnidar

    # Misc
    culture =  high_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

### Duchy of Ainethan ###
#County of Ainethan
72 = { #Ainethan

    # Misc
    culture =  high_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Oldport
38 = { #Oldport

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = city_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Casthil
73 = { #Casthil

    # Misc
    culture =  high_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}


### Duchy of Lorenith ###
#County of Lorenith
61 = { #Lorenith

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Redfort
68 = { #Redfort

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

### Duchy of Rosefield ###
#County of Rosefield
133 = { #Rosefield

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

71 = { #Ruby Pass

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#Province of Lasean
158 = { #Lasean

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#Province of Ar Esta
152 = { #Ar Esta

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

### Duchy of Lower Bloodwine ###
#County of Minar
97 = { #Minar

    # Misc
    culture = low_lorentish
    religion = house_of_minara
    holding = church_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Foxalley
81 = { #Foxalley

    # Misc
    culture = low_lorentish
    religion = house_of_minara
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Wineport
101 = { #Wineport

    # Misc
    culture = low_lorentish
    religion = house_of_minara
    holding = city_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Lower Bloodwine
79 = { #Lower Bloodwine

    # Misc
    culture =  low_lorentish
    religion = house_of_minara
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Pircost
80 = {

    # Misc
    culture = low_lorentish
    religion = house_of_minara
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

### Duchy of Horsegarden ###
#County of Horsegarden
105 = { #Horsegarden

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Greatfield
78 = { #Greatfield

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

### Duchy of Rewanwood ###
#County of Gladegate
77 = { #Gladegate

    # Misc
    culture =  moon_elf
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Rewanwood
119 = { #Rewanwood

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Wesmar
87 = { #Wesmar

    # Misc
    culture =  low_lorentish
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}
